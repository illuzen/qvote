
Prototype contract for QVote <br>
`npm install -g truffle`<br>
`npm install -g ganache-cli`<br>
`npm install`<br>

`ganache-cli -i 333 -m 'robot spawn entry venue code forest crouch glow suit barrel attract right'`<br>
`npm run deploy`<br>
`npm start`<br>

This runs a toy blockchain with network id 333 and a fixed mnemonic, so you can have deterministic development.    
<br>
In metamask, make sure you are on the localhost network, and add private keys <br>
0xb2d113b2b3e5ade34c925b961e97ce932dd40e9efaaf2ac179eeb30937d8845e as "QVote-dev management" <br>
0xf4c76d516afc8d657b87034767fbee545d6bd2233dbe1bf7e767b95d81a1d0eb as "QVote-dev user"


Check out truffle/migrations/2_generate_events.js for the auto generated topics and distribution of VOX. Hashes are gotten from adding the required json structure to ipfs. Easiest way is save to file and upload via app.hiplending.com<br>

You can always check the contents of a hash by going to https://ipfs.io/ipfs/QmSZTEX5cpPRdB23T18PrTj1BAxFLwrLp6rfiBiqs6mq4G<br>

To distribute more VOX manually, after deploying, 
`cd truffle`<br>
`truffle console --network development`<br>
`qvote = await QVote.deployed()`<br>
`qvote.mint(...)`<br>

If you need to reset for any reason, just redo `npm run deploy`.



