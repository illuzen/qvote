
import {
    getIPFS,
    getIPFSFromBytes
} from "./ipfs"

import { setupENS } from '@ensdomains/ui'

import {
    bytesToString,
    getTruffleContractInfura,
} from "./ethereum.js"

let ENS;

export async function getTokenList(ensAddr) {
    if (!ENS) {
        const { ens } = await setupENS()
        ENS = ens;
    }
    const content = await ENS.getContent(ensAddr)
    const parsed = content.value.slice(7)
    console.log('content', parsed)
    // chop off the ipfs://
    const list = await getIPFS(parsed)
    return list.tokens
}

export async function getTournament(tournamentId) {
    const cntrct = await getTruffleContractInfura();
    return await cntrct.tournaments(tournamentId);
}

export async function decodeTournament(tournament) {
    const o = {};
    o.metadata = await getIPFSFromBytes(tournament.metadataHash);
    o.startBlock = tournament.startBlock.toNumber();
    o.roundLengthBlocks = tournament.roundLengthBlocks.toNumber();
    o.finRoundBonus = tournament.finRoundBonus.toNumber();
    const ensAddr = bytesToString(tournament.tokenListENS);
    console.log('ensAddr', ensAddr)
    o.tokenList = await getTokenList(ensAddr);
    console.log('o', o)

    return o;
}

export async function getVoiceCredits(tournamentId, user) {
    const cntrct = await getTruffleContractInfura();
    return await cntrct.getVoiceCredits(tournamentId, user);
}