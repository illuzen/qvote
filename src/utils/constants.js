export const USE_METAMASK = true;
// this is not sensitive, since we can bind to our contract
// export const ETH_PROVIDER_URL = 'wss://mainnet.infura.io/ws/v3/d502b01776694b3197d82c289d8a60e8';
export const ETH_PROVIDER_URL = 'ws://localhost:8545';
export const IPFS_GATEWAY_URL = 'https://ipfs.io/ipfs/';
export const ETHERSCAN_STEM = 'https://etherscan.io/tx/';
export const TOPIC_INPUTS = [];
export const VOTE_INPUTS = [];

