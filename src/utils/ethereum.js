import Web3, {utils} from "web3";
//import QVote from "./contracts/QVote.json";
import Trollbox from "../contracts/Trollbox.json";
import {
  USE_METAMASK,
  ETH_PROVIDER_URL,
  TOPIC_INPUTS,
  VOTE_INPUTS
} from "./constants";

const truffleContract = require("@truffle/contract");


let infuraContract, truffleContractInfura, truffleContractMetamask;
let contractAbi = Trollbox;
let topics = [];
let votes = [];
export { topics, votes };

let metamaskWeb3;
const infuraWeb3 = new Web3(new Web3.providers.WebsocketProvider(ETH_PROVIDER_URL));

console.log('UsininfuraContractg web3 version: ', infuraWeb3.version);


window.addEventListener('load', async () => {
  if (window.ethereum && typeof window.web3 !== 'undefined' && USE_METAMASK) {
    await window.ethereum.enable();
    metamaskWeb3 = new Web3(window.web3.currentProvider);
    console.log('metamask injecting web3', metamaskWeb3)
  } else {
    metamaskWeb3 = infuraWeb3;
    console.log('web3 not injected');
  }
});

const topicSig = utils.keccak256('TopicCreated(uint256,uint256,address,bytes32)');
const voteSig = utils.keccak256('VoteOccurred(uint256,address,uint256)');

export async function isUserManagement() {
  let isManagement = false;
  if (metamaskWeb3) {
    const accounts = await metamaskWeb3.eth.getAccounts();
    console.log('accounts', accounts);
    const metamaskAddr = accounts[0];
    const managementAddr = await getManagementAddress();
    isManagement = metamaskAddr === managementAddr;
    console.log(metamaskAddr, managementAddr)
  }
  return isManagement;
}

async function getManagementAddress() {
  const lm = await getTruffleContractInfura();
  const ma = await lm.management();
  return ma;
}





export async function getAllEvents(
  callback,
  fromBlock = 0,
  toBlock = "latest",
) {
//  getAllTopicEvents(callback, fromBlock, toBlock);
//  getAllVoteEvents(callback, fromBlock, toBlock);
}

export function subscribeLogEvent(contract, eventName, eventSig, eventInputs, callback) {
  infuraWeb3.eth.subscribe('logs', {
    address: contract.address,
    topics: [eventSig]
  }, (error, result) => {
    if (!error) {
      const eventObj = infuraWeb3.eth.abi.decodeLog(
        eventInputs,
        result.data,
        result.topics.slice(1)
      );
      eventObj.txHash = result.transactionHash;
      callback(eventObj);
    } else {
      console.error(`Error making subscription to ${eventName} on ${contract.address}: ${error}`);
    }
  })
}



export async function getInfuraContract() {
  if (!infuraContract) {
    let networkId;
    await infuraWeb3.eth.net.getId((err, id) => { networkId = id; });
    console.log('networkId', networkId)
    const contractAddress = contractAbi.networks[networkId].address;
    console.log('contract address', contractAddress)
    infuraContract = new infuraWeb3.eth.Contract(contractAbi.abi, contractAddress);
    console.log(infuraContract)
    // glm.setProvider(new web3.providers.WebsocketProvider(WEBSOCKET_PROVIDER));
  }
  return infuraContract;
}

export async function getTruffleContractMetamask() {
  if (!truffleContractMetamask) {
    const tglm = truffleContract(contractAbi);
    tglm.setProvider(metamaskWeb3.currentProvider);
    truffleContractMetamask = await tglm.deployed();
  }
  return truffleContractMetamask;
}

export async function getTruffleContractInfura() {
  if (!truffleContractInfura) {
    const tglm = truffleContract(contractAbi);
    tglm.setProvider(infuraWeb3.currentProvider);
    truffleContractInfura = await tglm.deployed();
  }
  return truffleContractInfura;
}


export async function getEthAccount() {
  let accounts, ethAccount;
  if (metamaskWeb3) {
    accounts = await metamaskWeb3.eth.getAccounts();
  } else {
    accounts = await infuraWeb3.eth.getAccounts();
  }
  ethAccount = accounts[0];
  if (!ethAccount) {
    const msg = 'Please ensure that metamask is installed and activated to use this feature';
    console.error(msg);
    alert(msg);
    throw msg;
  }
  return ethAccount;
}

export function bytesToString(bytes) {
    return Web3.utils.hexToString(bytes);
}
