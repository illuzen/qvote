
import {
  TOPIC_INPUTS,
  VOTE_INPUTS
} from "./constants";

import {
    addIPFS,
    getIPFS,
} from './ipfs.js';

export async function createTopic(name, content) {
  const hash = await addIPFS({
    'version': 1,
    'name': name,
    'content': content
  });
  await createTopicBlockchain(0, hash);
}

export async function createTopicBlockchain(refId, hash) {
  const qVote = await getTruffleContractMetamask();
  const ethAccount = await getEthAccount();
  const ethBytes = getEthBytesFromIpfsHash(hash);
  await qVote.createTopic(refId, ethBytes, {from:ethAccount, gas: 150e3, gasPrice: 8e9});
}

export async function getAllVoteEvents(
  callback,
  fromBlock = 0,
  toBlock = "latest",
) {
  const web3QVote = await getInfuraContract();
  const truffleQVote = await getTruffleContractInfura();
  const filter = { fromBlock: fromBlock, toBlock: toBlock };

  web3QVote.events.VoteOccurred(filter, async (error, response) => {
    console.log('past vote event found', response);
    if (error) {
      console.error(error);
      return;
    }
    response.returnValues.txHash = response.transactionHash;
    processVoteEvent(response.returnValues, callback);
  });
  subscribeLogEvent(truffleQVote, 'VoteOccurred', voteSig, VOTE_INPUTS,(event) => {
    processVoteEvent(event, callback);
  });

}

export async function getAllTopicEvents(
  callback,
  fromBlock = 0,
  toBlock = 'latest'
) {
  const web3QVote = await getInfuraContract();
  const truffleQVote = await getTruffleContractInfura();
  const filter = { fromBlock: fromBlock, toBlock: toBlock };
//  web3QVote.events.TopicCreated(filter, async (error, response) => {
//    console.log('past topic event found', response);
//    if (error) {
//      console.error(error);
//      return;
//    }
//    response.returnValues.txHash = response.transactionHash;
//    processTopicEvent(response.returnValues, callback);
//  });
  subscribeLogEvent(truffleQVote, 'TopicCreated', topicSig, TOPIC_INPUTS,(event) => {
    processTopicEvent(event, callback);
  });

}


export async function processTopicEvent(event, callback) {
  const truffleQVote = await getTruffleContractInfura();
  console.log('event', event);
  if (!event.topicId) return;
  const topic = await truffleQVote.topicMap(event.topicId);
  const ipfsData = await getIPFS(topic.descriptionHash);
  const merged = {
    ...topic,
    ...ipfsData,
    votes: topic.votes.toNumber()
  };
  console.log('Processing topic ', merged);
  topics.push(merged);
  callback(merged);
}

export function processVoteEvent(event, callback) {
  console.log('Processing vote', event)
  if (!event.topicId) return;
  callback(event);
}

// assume csv is list of dictionaries
export async function vote(topicId, numVotes) {
  const qVote = await getTruffleContractMetamask();
  const ethAccount = await getEthAccount();
  await qVote.vote(topicId, numVotes, {from:ethAccount, gas: 100e3, gasPrice: 8e9});
}

export async function getVoxBalance() {
  const qVote = await getTruffleContractMetamask();
  const ethAccount = await getEthAccount();
  const balance = await qVote.balanceOf(ethAccount);
  console.log('vox balance', balance.toNumber());
  return balance;



export async function alreadyVoted(topicId) {
  const qVote = await getTruffleContractMetamask();
  const ethAccount = await getEthAccount();
  const voted = await qVote.alreadyVoted(ethAccount, topicId);
  console.log('alreadyVoted', voted);
  return voted;
}
