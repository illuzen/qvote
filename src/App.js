import React, { Component } from "react";
import logo from './logo.png';
import './App.css';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import {Popconfirm, Table, Divider} from "antd";

import {
  getAllTopicEvents,
  getAllVoteEvents,
  isUserManagement,
  vote,
  createTopic,
  alreadyVoted,
  getIPFS,
  bytesToString,
} from "./utils/ethereum.js";

import {
  decodeTournament,
  getTournament,
} from "./utils/fin.js";

const columns = [
    {
        title: '',
        dataIndex: 'logo',
        key: 'logo',
        editable: false,
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        editable: false,
    },
    {
        title: 'Votes',
        dataIndex: 'votes',
        key: 'votes',
        editable: false,
    },
    {
        title: '7d change',
        dataIndex: '7d change',
        key: '7d change',
        editable: false,
    },
]

const data = [
    {
        'key': 'btc',
        'logo': 'logo',
        'name': 'Bitcoin',
        'votes': '145',
        '7d change': '+45%'
    },
    {
        'key': 'eth',
        'logo': 'logo',
        'name': 'Ethereum',
        'votes': '102',
        '7d change': '+15%'
    },
    {
        'key': 'dash',
        'logo': 'logo',
        'name': 'Dash',
        'votes': '34',
        '7d change': '-5%'
    },
]

class App extends Component {
  state = {
    currentTournament: {} ,
    numVotes: 0,
    dialog: {
      show:false,
      title: '',
      content: '',
      topicId: 0,
      alreadyVoted: false
    },
    voxBalance: 0,
    isManagement: false,
  };

  updateTournament = async () => {
    console.log('getting tournament 1')
    const t = await getTournament(1);
    console.log('decoding tournament 1')
    const decoded = await decodeTournament(t)

    this.setState((state, props) => ({
        currentTournament: decoded
    }))

  }

//  updateVoxBalance = async () => {
//    const balance = await getVoxBalance();
//    this.setState((state, props) => ({
//      voxBalance: balance.toNumber()
//    }));
//  };

  componentDidMount = async () => {


      setTimeout(this.updateTournament, 1000);

//    getAllTopicEvents((topic) => {
//      this.setState((state, props) => ({
//        topics: {
//          ...state.topics,
//          [topic.topicId]: topic
//        }
//      }));
//    });
//    getAllVoteEvents((vote) => {
//      console.log('vote', vote)
//      this.setState((state, props) => ({
//        topics: {
//          ...state.topics,
//          [vote.topicId]: {
//            ...state.topics[vote.topicId],
//            votes: vote.totalVotes
//          }
//        }
//      }));
//    });

//    const mgmt = await isUserManagement();
//    this.setState((state, props) => ({
//      isManagement: mgmt,
//    }));
//    setTimeout(this.updateVoxBalance, 1000);
//    setInterval(this.updateVoxBalance,10000);
  };


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <Table
            dataSource={this.state.currentTournament.tokenList}
            columns={columns}
           />
        </header>
      </div>
    );
  }
}

export default App;
