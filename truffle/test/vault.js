'use strict';

const Web3 = require('web3');
const web3 = new Web3("ws://localhost:8545");
const bN = web3.utils.toBN;
const l = console.log;
const Vault = artifacts.require('Vault');
const Fin = artifacts.require('FIN');
const utils = require('./utils/utils')

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');


const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function assertEvents(promise, eventNames) {
    let rcpt = await promise;
    if (typeof (rcpt) === 'string') {
        rcpt = await web3.eth.getTransactionReceipt(rcpt);
    }

    assert(rcpt.logs.length === eventNames.length);
    for (let i = 0; i < rcpt.logs.length; i++) {
        assert(rcpt.logs[i].event === eventNames[i]);
    }
    return rcpt;
}

async function assertReverts(fxn, args) {
    try {
        await fxn(args);
        assert(false);
    } catch (e) {
        //
    }
}

async function increaseTime(bySeconds) {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

async function mineBlocks(numBlocks) {
    for (var i = 0; i < numBlocks; i++) {
        await mineOneBlock();
    }
}

async function getChainHeight() {
    const tip = await web3.eth.getBlock('latest')
    return new BN(tip.number);
}


contract('Vault', function (accounts) {
    const state = {};
    const dmfAddress = accounts[2];
    const advisorsAddress = accounts[3];
    const investorsAddress = accounts[4];
    const teamAddress = accounts[5];
    const decimals = new BN('1000000000000000000')
    const investorAllocation = decimals.mul(new BN(200000000));
    const dmfAllocation = decimals.mul(new BN(600000000));
    const advisorsAllocation = decimals.mul(new BN(50000000));
    const teamAllocation = decimals.mul(new BN(150000000));

    async function expectedWithdrawalAmount(trancheId) {
        const tranche = await state.vault.tranches(trancheId);
        const startBlock = tranche[7];
        const coinsPerBlock = tranche[2].div((tranche[5].sub(startBlock)))
        const lastWithdrawalDate = tranche[6];
        const vestingPeriodEnd = tranche[5];
        let chainHeight = (await getChainHeight()).add(new BN(1));
        if (chainHeight.gt(vestingPeriodEnd)) {
            chainHeight = vestingPeriodEnd;
        }
        const withdrawalAmount = chainHeight.sub(lastWithdrawalDate).mul(coinsPerBlock);
        return withdrawalAmount;
    }

    async function checkWithdrawal(trancheId) {
        const trancheBefore = await state.vault.tranches(trancheId);
        const destination = trancheBefore[1];

        const currentCoinsBefore = trancheBefore[3];
        const destinationBalanceBefore = await state.fin.balanceOf(destination);
        const vaultBalanceBefore = await state.fin.balanceOf(state.vault.address);

        const withdrawalAmount = await expectedWithdrawalAmount(trancheId);
//        l('expectedWithdraw amount', withdrawalAmount.toString())
        await state.vault.withdraw(trancheId);

        const trancheAfter = await state.vault.tranches(trancheId);

        const currentCoinsAfter = trancheAfter[3];
        const destinationBalanceAfter = await state.fin.balanceOf(destination);
        const vaultBalanceAfter = await state.fin.balanceOf(state.vault.address);

        expect(vaultBalanceBefore.sub(vaultBalanceAfter)).to.eq.BN(withdrawalAmount);
        expect(destinationBalanceAfter.sub(destinationBalanceBefore)).to.eq.BN(withdrawalAmount);
        expect(currentCoinsBefore.sub(currentCoinsAfter)).to.eq.BN(withdrawalAmount);

        return withdrawalAmount
    }

    beforeEach(async () => {
        state.centralBank = accounts[0];
        state.initialBalance = new BN(1e9);
        state.fin = await Fin.new(state.centralBank, state.initialBalance);
        state.vault = await Vault.new(dmfAddress, advisorsAddress, investorsAddress, teamAddress, state.fin.address);
        await state.fin.transfer(state.vault.address, state.initialBalance.mul(decimals), {from: state.centralBank});
    });

    afterEach(() => {});

    it('it initializes correctly', async function () {
        const dmfTranche = await state.vault.tranches(1);
        const advisorsTranche = await state.vault.tranches(2);
        const investorsTranche = await state.vault.tranches(3);
        const teamTranche = await state.vault.tranches(4);

        expect(dmfTranche[1]).to.be.a('string').that.equals(dmfAddress);
        expect(advisorsTranche[1]).to.be.a('string').that.equals(advisorsAddress);
        expect(investorsTranche[1]).to.be.a('string').that.equals(investorsAddress);
        expect(teamTranche[1]).to.be.a('string').that.equals(teamAddress);

        expect(dmfTranche[2]).to.eq.BN(dmfAllocation);
        expect(advisorsTranche[2]).to.eq.BN(advisorsAllocation);
        expect(investorsTranche[2]).to.eq.BN(investorAllocation);
        expect(teamTranche[2]).to.eq.BN(teamAllocation);

        expect(dmfTranche[3]).to.eq.BN(dmfAllocation);
        expect(advisorsTranche[3]).to.eq.BN(advisorsAllocation);
        expect(investorsTranche[3]).to.eq.BN(investorAllocation);
        expect(teamTranche[3]).to.eq.BN(teamAllocation);
    });

    it('it allows investors to withdraw 25% of their funds immediately', async function () {
        await mineBlocks(34);
        await checkWithdrawal(3);
    });

    it('it allows token holders to withdraw 100% of their funds after vesting', async function () {
        await mineBlocks(300);
        const dmfWithdrawal = await checkWithdrawal(1)
        const advisorsWithdrawal = await checkWithdrawal(2)
        const investorsWithdrawal = await checkWithdrawal(3)
        const teamWithdrawal = await checkWithdrawal(4)

        expect(dmfWithdrawal).to.eq.BN(dmfAllocation);
        expect(advisorsWithdrawal).to.eq.BN(advisorsAllocation);
        expect(investorsWithdrawal).to.eq.BN(investorAllocation);
        expect(teamWithdrawal).to.eq.BN(teamAllocation);
    });

    it('it disallows token holders to withdraw any funds before lock time expires', async function () {
        await mineBlocks(1);
        assertReverts(state.vault.withdraw, [1])
        assertReverts(state.vault.withdraw, [2])
        assertReverts(state.vault.withdraw, [4])
        await mineBlocks(100);
        await state.vault.withdraw(1);
        await state.vault.withdraw(3);
        await state.vault.withdraw(4);
    });

    it('it allows you to break up withdrawals into multiple', async function () {
        let sum = new BN(0);
        await mineBlocks(101);
        for (var i = 0; i < 10; i++) {
            sum = sum.add(await checkWithdrawal(1))
            await mineBlocks(10);
        }
        expect(sum).to.eq.BN(dmfAllocation);
    });

});