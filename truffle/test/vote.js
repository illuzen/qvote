'use strict';

const Web3 = require('web3');
const web3 = new Web3("ws://localhost:8545");
const bN = web3.utils.toBN;
const l = console.log;
const Trollbox = artifacts.require('Trollbox');
const Fin = artifacts.require('FIN');

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { ZERO_BYTES32 } = constants; // empty hash


const chai = require('chai');
const bnChai = require('bn-chai');
chai.use(bnChai(BN));
chai.config.includeStack = true;
const expect = chai.expect;

async function assertEvents(promise, eventNames) {
    let rcpt = await promise;
    if (typeof (rcpt) === 'string') {
        rcpt = await web3.eth.getTransactionReceipt(rcpt);
    }

    assert(rcpt.logs.length === eventNames.length);
    for (let i = 0; i < rcpt.logs.length; i++) {
        assert(rcpt.logs[i].event === eventNames[i]);
    }
    return rcpt;
}

async function assertReverts(fxn, args) {
    try {
        await fxn(args);
        assert(false);
    } catch (e) {
        //
    }
}

async function increaseTime(bySeconds) {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_increaseTime',
        params: [bySeconds],
        id: new Date().getTime(),
    }, (err, result) => {
      if (err) { console.error(err) }

    });
    await mineOneBlock();
}

async function mineOneBlock() {
    await web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: new Date().getTime(),
    }, () => {});
}

async function mineBlocks(numBlocks) {
    for (var i = 0; i < numBlocks; i++) {
        await mineOneBlock();
    }
}

async function getChainHeight() {
    const tip = await web3.eth.getBlock('latest')
    return new BN(tip.number);
}


contract('Trollbox', function (accounts) {
    const state = {};
    const decimals = new BN('1000000000000000000')

    async function createTournament() {
        const hash =  '0x2222222222222222222222222222222222222222222222222222222222222222'
        const chainHeight = await getChainHeight()
        const startBlock = chainHeight.add(new BN(10));
        const roundLengthBlocks = new BN(5);
        const finRoundBonus = new BN(100);
        const tokenListENS = '0x1111111111111111111111111111111111111111111111111111111111111111'
        const args = [hash, startBlock,roundLengthBlocks, finRoundBonus, tokenListENS]
        await state.trollbox.createTournament(...args, {from: state.mgmt})
    }

    beforeEach(async () => {
        state.mgmt = accounts[0];
        state.oracle = accounts[1];
        state.initialBalance = new BN(1e9);
        state.fin = await Fin.new(state.mgmt, state.initialBalance);
        state.trollbox = await Trollbox.new(state.mgmt, state.oracle, state.fin.address);
    });

    afterEach(() => {});

    it('only mgmt can change mgmt key', async function () {
        const notMgmt = accounts[2];
        const contractMgmtBefore = await state.trollbox.management();
        assertReverts(state.trollbox.setManagement, [notMgmt, {from: notMgmt}])
        const contractMgmtAfter = await state.trollbox.management();
        expect(contractMgmtAfter).to.be.a('string').that.equals(contractMgmtBefore);
        await state.trollbox.setManagement(notMgmt, {from: contractMgmtBefore})
        const contractMgmtAfter2 = await state.trollbox.management();
        expect(contractMgmtAfter2).to.be.a('string').that.equals(notMgmt);
    })

    it('only mgmt can change oracle key', async function () {
        const notMgmt = accounts[2];
        const notOracle = accounts[3];
        const contractOracleBefore = await state.trollbox.winnerOracle();
        assertReverts(state.trollbox.setWinnerOracle, [notOracle, {from: notMgmt}])
        const contractOracleAfter = await state.trollbox.winnerOracle();
        expect(contractOracleAfter).to.be.a('string').that.equals(contractOracleBefore);
        await state.trollbox.setWinnerOracle(notOracle, {from: state.mgmt})
        const contractOracleAfter2 = await state.trollbox.winnerOracle();
        expect(contractOracleAfter2).to.be.a('string').that.equals(notOracle);
    })

    it('only mgmt can change voice ubi', async function () {
        const notMgmt = accounts[2];
        const newUbi = new BN(101);
        const contractUbiBefore = await state.trollbox.voiceUBI();
        assertReverts(state.trollbox.setVoiceUBI, [newUbi, {from: notMgmt}])
        const contractUbiAfter = await state.trollbox.voiceUBI();
        expect(contractUbiAfter).to.eq.BN(contractUbiBefore);
        await state.trollbox.setVoiceUBI(newUbi, {from: state.mgmt})
        const contractUbiAfter2 = await state.trollbox.voiceUBI();
        expect(contractUbiAfter2).to.eq.BN(newUbi);
    })

    it('only mgmt can change identity cost', async function () {
        const notMgmt = accounts[2];
        const newIdCost = new BN(101);
        const contractIdCostBefore = await state.trollbox.identityCost();
        assertReverts(state.trollbox.setIdentityCost, [newIdCost, {from: notMgmt}])
        const contractIdCostAfter = await state.trollbox.identityCost();
        expect(contractIdCostAfter).to.eq.BN(contractIdCostBefore);
        await state.trollbox.setIdentityCost(newIdCost, {from: state.mgmt})
        const contractIdCostAfter2 = await state.trollbox.identityCost();
        expect(contractIdCostAfter2).to.eq.BN(newIdCost);
    })

    it('anybody can create their identity by burning fin', async function()  {
        const finfulAddr = accounts[0]
        const noFinAddr = accounts[1]
        const idCost = await state.trollbox.identityCost();
        await state.fin.increaseApproval(state.trollbox.address, idCost, {from: noFinAddr});
        await state.fin.increaseApproval(state.trollbox.address, idCost, {from: finfulAddr});
        assertReverts(state.trollbox.createMyIdentity, [{from: noFinAddr}]);
        await state.trollbox.createMyIdentity({from: finfulAddr});
        const noFinId = await state.trollbox.identities(noFinAddr)
        const finfulId = await state.trollbox.identities(finfulAddr)
        expect(noFinId).to.eq.BN(new BN(0))
        expect(finfulId).to.be.gt.BN(0)
    })

    it('only mgmt can  create an identity for someone else for free', async function() {
        const notMgmt = accounts[2];
        const noFinAddr = accounts[1];
        const contractNoFinIdBefore = await state.trollbox.identities(noFinAddr);
        assertReverts(state.trollbox.createIdentityFor, [noFinAddr, {from: notMgmt}]);
        const contractNoFinIdAfter = await state.trollbox.identities(noFinAddr);
        expect(contractNoFinIdAfter).to.eq.BN(contractNoFinIdBefore);
        await state.trollbox.createIdentityFor(noFinAddr, {from: state.mgmt})
        const contractNoFinIdAfter2 = await state.trollbox.identities(noFinAddr);
        expect(contractNoFinIdAfter2).to.be.gt.BN(0)
    })

    it('only mgmt can create a tournament', async function() {
        const notMgmt = accounts[2];
        const tournId = new BN(1);
        const contractTournBefore = await state.trollbox.tournaments(tournId);
        expect(contractTournBefore.tournamentId).to.eq.BN(0);
        const hash =  '0x2222222222222222222222222222222222222222222222222222222222222222'
        const startBlock = new BN(10);
        const roundLengthBlocks = new BN(1);
        const finRoundBonus = new BN(100);
        const tokenListENS = '0x1111111111111111111111111111111111111111111111111111111111111111'
        const args = [hash, startBlock,roundLengthBlocks, finRoundBonus, tokenListENS]
        assertReverts(state.trollbox.createTournament, [...args, {from: notMgmt}])
        const contractTournAfter = await state.trollbox.tournaments(tournId);
        expect(contractTournAfter.tournamentId).to.eq.BN(0);
        await state.trollbox.createTournament(...args, {from: state.mgmt})
        const contractTournAfter2 = await state.trollbox.tournaments(tournId);
        expect(contractTournAfter2.tournamentId).to.eq.BN(1);
        expect(contractTournAfter2.metadataHash).to.be.a('string').that.equals(hash);
        expect(contractTournAfter2.startBlock).to.eq.BN(startBlock);
        expect(contractTournAfter2.roundLengthBlocks).to.eq.BN(roundLengthBlocks);
        expect(contractTournAfter2.finRoundBonus).to.eq.BN(finRoundBonus);
        expect(contractTournAfter2.tokenListENS).to.be.a('string').that.equals(tokenListENS);
    })

    it('only oracle is allowed to pick resolve rounds for tournament after 2 rounds', async function() {
        await createTournament();
        await mineBlocks(10);
        const notOracle = accounts[5]
        const [roundId1, winningOption1] = await state.trollbox.getRound(1, 1);
        expect(roundId1).to.eq.BN(0)
        expect(winningOption1).to.eq.BN(0)

        assertReverts(state.trollbox.resolveRound, [1, 1, 1, {from: notOracle}]);
        const [roundId2, winningOption2] = await state.trollbox.getRound(1, 1);
        expect(roundId2).to.eq.BN(0)
        expect(winningOption2).to.eq.BN(0)

        assertReverts(state.trollbox.resolveRound, [1, 1, 1, {from: state.oracle}]);
        const [roundId3, winningOption3] = await state.trollbox.getRound(1, 1);
        expect(roundId3).to.eq.BN(0)
        expect(winningOption3).to.eq.BN(0)

        await mineBlocks(10);
        assertReverts(state.trollbox.resolveRound, [1, 1, 1, {from: notOracle}]);
        const [roundId5, winningOption5] = await state.trollbox.getRound(1, 1);
        expect(roundId5).to.eq.BN(0)
        expect(winningOption5).to.eq.BN(0)

        await state.trollbox.resolveRound(1, 1, 1, {from: state.oracle});
        const [roundId4, winningOption4] = await state.trollbox.getRound(1, 1);
        expect(roundId4).to.eq.BN(1)
        expect(winningOption4).to.eq.BN(1)
    })

    it('voting allowed only by identities', async function() {
        await createTournament()
        await mineBlocks(10);
        const tournamentId = 1;
        const voter =  accounts[3];
        const currentRoundId = await state.trollbox.getCurrentRoundId(tournamentId);
        assertReverts(state.trollbox.vote, [tournamentId, [1,2,3], [1,2,3], {from: voter}])
        await state.trollbox.createIdentityFor(voter, {from: state.mgmt});
        await state.trollbox.vote(tournamentId, [1,2,3], [1,2,3], {from: voter});
    })

    it('round ids progress correctly', async function() {
        await createTournament();
        const roundId1 = await state.trollbox.getCurrentRoundId(1);
        expect(roundId1).to.eq.BN(0);
        await mineBlocks(10);
        const roundId2 = await state.trollbox.getCurrentRoundId(1);
        expect(roundId2).to.eq.BN(1);
        await mineBlocks(5);
        const roundId3 = await state.trollbox.getCurrentRoundId(1);
        expect(roundId3).to.eq.BN(2);
        await mineBlocks(5);
        const roundId4 = await state.trollbox.getCurrentRoundId(1);
        expect(roundId4).to.eq.BN(3);
    })

    it('voters get rewarded correctly', async function()  {
        const tournamentId = new BN(1);
        const correctVoter = accounts[3];
        const incorrectVoter = accounts[4];
        await state.trollbox.createIdentityFor(correctVoter, {from: state.mgmt});
        await state.trollbox.createIdentityFor(incorrectVoter, {from: state.mgmt});
        await createTournament();
        await mineBlocks(10);
        await state.trollbox.vote(tournamentId, [1], [10], {from: correctVoter});
        await state.trollbox.vote(tournamentId, [2], [10], {from: incorrectVoter});
        await mineBlocks(10);
        await state.trollbox.resolveRound(1, 1, 1, {from: state.oracle})
        const lastRoundVotedCorrect = await state.trollbox.getLastRoundVoted(tournamentId, correctVoter);
        const lastRoundVotedIncorrect = await state.trollbox.getLastRoundVoted(tournamentId, incorrectVoter);
        expect(lastRoundVotedCorrect).to.eq.BN(1);
        expect(lastRoundVotedIncorrect).to.eq.BN(1);
        const correctNums = await state.trollbox.getLastRoundReward(1, correctVoter);
        const incorrectNums = await state.trollbox.getLastRoundReward(1, incorrectVoter);
        expect(correctNums['0']).to.eq.BN(100)
        expect(correctNums['1']).to.eq.BN(100)
        expect(incorrectNums['0']).to.eq.BN(0)
        expect(incorrectNums['1']).to.eq.BN(100)
    })
})