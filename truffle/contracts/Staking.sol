pragma solidity ^0.5.0;

import "./FIN.sol";
import "./SafeMathLib.sol";
import "./Trollbox.sol";

contract Staking {
    using SafeMathLib for uint;

    struct Stake {
        bytes32 stakeId;
        uint tournamentId;
        uint roundId;
        uint winningOption;
        uint stakeAmount;
        mapping (address => uint) balances;
    }

    FIN public finToken;
    Trollbox public trollbox;
    uint public numStakes = 0;
    uint public roundReward;

    // hash(address,uint,uint)
    mapping (bytes32 => Stake) public stakeMap;

    constructor (address fin, address troll, uint reward) public {
        finToken = FIN(fin);
        trollbox = Trollbox(troll);
        roundReward = reward;
    }

    function getStakeId(address horse, uint tournamentId, uint roundId) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(horse, tournamentId, roundId));
    }

    function createStake(address horse, uint tournamentId, uint roundId) public {
        bytes32 stakeId = getStakeId(horse, tournamentId, roundId);
        Stake storage thisStake = stakeMap[stakeId];
        require(thisStake.stakeId == 0, 'Stake already created');

        require(trollbox.isIdentity(horse), "cannot bet on non-identity");
        numStakes = numStakes.plus(1);

        thisStake.stakeId = stakeId;
        thisStake.tournamentId = tournamentId;
        thisStake.roundId = roundId;
    }

    function stake(address horse, uint tournamentId, uint roundId, uint amount) public {
        bytes32 stakeId = getStakeId(horse, tournamentId, roundId);
        finToken.transferFrom(msg.sender, address(this), amount);
        Stake storage thisStake = stakeMap[stakeId];
        thisStake.stakeAmount = thisStake.stakeAmount.plus(amount);
        thisStake.balances[msg.sender] = thisStake.balances[msg.sender].plus(amount);
    }

    function withdraw(address horse, uint tournamentId, uint roundId) public {
        bytes32 stakeId = getStakeId(horse, tournamentId, roundId);
        Stake storage thisStake = stakeMap[stakeId];
        require(thisStake.balances[msg.sender] > 0, 'No stake to withdraw');
        if (thisStake.winningOption == 0) {
            uint[2] memory details = trollbox.getRound(thisStake.tournamentId, thisStake.roundId);
            require(details[1] > 0, 'Round not resolved yet');
            thisStake.winningOption = details[1];
        }

        uint pool = roundReward.plus(thisStake.stakeAmount);
        uint withdrawAmount = pool.times(thisStake.balances[msg.sender]) / thisStake.stakeAmount;

        thisStake.balances[msg.sender] = 0;

        finToken.transfer(msg.sender, withdrawAmount);
    }
}
