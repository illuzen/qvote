pragma solidity ^0.5.0;

interface ENS {
    function resolver(bytes32 node) external view returns (address);
}