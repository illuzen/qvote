pragma solidity ^0.5.0;

import "./SafeMathLib.sol";
import "./FIN.sol";


contract Trollbox {
    using SafeMathLib for uint;

    /**
        Votes are a mapping from choices to weights, plus a metadataHash, which references an arbitrary bit of metadata
        stored on IPFS. The meaning of these choices is not stored on chain, only the index. For example, if  the choices
        are ["BTC", "ETH", "DASH"],  and the user  wants to put 3 votes on BTC, 5 votes on ETH and 4 on DASH, then this
        will be recorded as weights[1]  = 3; weights[2]  = 5; weights[3] = 4; The choices are indexed starting on 1 to
        prevent confusion caused by empty votes.
    **/
    struct Vote {
        mapping(uint => int) weights;
        bytes32 metadataHash;
    }

    /**
        Rounds occur with some frequency and represent a complete cycle of prediction->resolution. Each round has an id,
        which represents it's location in a linear sequence of rounds of the same type. It stores a mapping of voter
        addresses to votes and records the winning option when the round is resolved.
    **/
    struct Round {
        uint roundId;
        mapping (address => Vote) votes;
        mapping (uint => uint) voiceTotals;
        uint winningOption;
    }

    /**
        A tournament is a linear sequence of rounds of the same type. Tournaments are identified by an integer that
        increases sequentially with each tournament. Tournaments also have hash for storing off-chain metadata about the
        tournament. A tournament has a set wavelength and phase, called roundLengthBlocks and startDate, respectively. Each
        tournament also has it's own set of voice credits, which is a mapping from address to balance. The rounds
        mapping takes a round id and spits out a Round struct. The finRoundBonus attribute describes how much FIN to be
        distributed to the voters each round. The tokenListENS stores the ENS address of a token list that forms the
        choices of the tournament.
    **/
    struct Tournament {
        uint tournamentId;
        bytes32 metadataHash;  // ipfs hash of more verbose description, possibly multimedia
        uint startBlock;
        uint roundLengthBlocks;
        uint finRoundBonus;
        bytes32 tokenListENS;
        mapping (address => uint) voiceCredits;
        mapping (uint => Round) rounds;
    }

    /**
        An identity is purchased with FIN and stores the creation time and a mapping of tournament id to the last round
        id that the identity voted in, which is used for deferred reward computation.
    **/
    struct Identity {
        mapping (uint => uint) lastRoundVoted;
        uint createdTime;
    }

    address public management; // authoritative key that can make important decisions, can be DAO address later
    address public winnerOracle; // address that sets the winner for a tournament
    FIN public finToken;
    uint public identityCost = 100; // burn cost of making an identity, in FIN
    uint public voiceUBI = 100; // number of voice credits available to spend each round
    uint public numTournaments = 0; // a counter to know what index to assign to new tournaments

    mapping (uint => Tournament) public tournaments; // mapping from tournament id to tournament struct
    mapping (address => Identity) public identities; // mapping from address to identity struct

    // events for consumption by off chain systems
    event VoteOccurred(uint indexed tournamentId, uint indexed roundId, address indexed voter, uint[] choices, int[] weights);
    event IdentityCreated(address indexed id);
    event RoundResolved(uint indexed tournamentId, uint roundId, uint winningChoice);
    event TournamentCreated(bytes32 metadataHash, uint startBlock, uint tournamentId, uint roundLengthBlocks, uint finRoundBonus);
    event ManagementUpdated(address oldManagement, address newManagement);
    event WinnerOracleUpdated(address oldOracle, address newOracle);
    event VoiceUBIUpdated(uint oldVoiceUBI, uint newVoiceUBI);
    event IdentityCostUpdated(uint oldIdCost, uint newIdCost);

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(address mgmt, address oracle, address fin) public {
        management = mgmt;
        winnerOracle = oracle;
        finToken = FIN(fin); // should not need to change this address
    }

    // change the management key
    function setManagement(address newMgmt) public managementOnly {
        address oldMgmt =  management;
        management = newMgmt;
        emit ManagementUpdated(oldMgmt, newMgmt);
    }

    // change who decides who the winner is
    function setWinnerOracle(address newOracle) public managementOnly {
        address oldOracle = winnerOracle;
        winnerOracle = newOracle;
        emit WinnerOracleUpdated(oldOracle, newOracle);
    }

    // set the amount of free voice credits per round
    function setVoiceUBI(uint newVoiceUBI) public managementOnly {
        uint oldVoiceUBI = voiceUBI;
        voiceUBI = newVoiceUBI;
        emit VoiceUBIUpdated(oldVoiceUBI, newVoiceUBI);
    }

    function setIdentityCost(uint newIdCost) public managementOnly {
        uint oldIdCost = identityCost;
        identityCost = newIdCost;
        emit IdentityCostUpdated(oldIdCost, newIdCost);
    }

    // this function creates an identity by burning the FIN. Anyone can call it.
    function createMyIdentity() public {
        finToken.burnFor(msg.sender, identityCost);
        createIdentity(msg.sender);
    }

    // this function creates an identity for free. Only management can call it.
    function createIdentityFor(address newId) public managementOnly {
        createIdentity(newId);
    }

    function createIdentity(address newId) internal {
        Identity storage id = identities[newId];
        require(id.createdTime == 0, 'Identity already created');
        id.createdTime = now;
        emit IdentityCreated(newId);
    }

    // this function creates a new tournament type, only management can call it
    function createTournament(bytes32 hash, uint startBlock, uint roundLengthBlocks, uint finRoundBonus, bytes32 tokenListENS) public managementOnly {
        numTournaments = numTournaments.plus(1);
        Tournament storage tournament = tournaments[numTournaments];
        tournament.metadataHash = hash;
        tournament.startBlock = startBlock;
        tournament.tournamentId = numTournaments;
        tournament.roundLengthBlocks = roundLengthBlocks;
        tournament.finRoundBonus = finRoundBonus;
        tournament.tokenListENS = tokenListENS;
        emit TournamentCreated(hash, startBlock, numTournaments, roundLengthBlocks, finRoundBonus);
    }

    // this allows management to update the metadata associated with the tournament
//    function updateTournament(uint tournamentId, bytes32 newHash) public managementOnly {
//        Tournament storage tournament = tournaments[tournamentId];
//        tournament.metadataHash = newHash;
//    }


    // this actually updates the voice credit balance to include the reward
    function updateVoiceBalance(uint tournamentId, address ofAddress) internal {
        Tournament storage tournament = tournaments[tournamentId];
        (uint voiceReward, uint totalVoice) = getLastRoundReward(tournamentId, ofAddress);
        if (totalVoice > 0) {
            uint finBonus = voiceReward.times(tournament.finRoundBonus) / totalVoice;
            finToken.transfer(ofAddress, finBonus);
        }
        tournament.voiceCredits[ofAddress] = voiceReward.plus(voiceUBI);
    }

    // this computes the id of the current round for a given tournament, starting with round 1 on the startBlock
    function getCurrentRoundId(uint tournamentId) public view returns (uint) {
        Tournament storage tournament = tournaments[tournamentId];
        uint startBlock = tournament.startBlock;
        uint roundLengthBlocks = tournament.roundLengthBlocks;
        if (block.number > startBlock) {
            return 1 + ((block.number - startBlock) / roundLengthBlocks);
        } else {
            return 0;
        }
    }

    // this completes the round, and assigns it a winning choice, which enables deferred updates to voice credits
    function resolveRound(uint tournamentId, uint roundId, uint winningOption) public {
        require(msg.sender ==  winnerOracle, 'Only winner oracle can call this');
        uint currentRoundId = getCurrentRoundId(tournamentId);
        Round storage round = tournaments[tournamentId].rounds[roundId];
        require(round.winningOption == 0, 'Round already resolved');
        require(currentRoundId > roundId + 1);
        round.roundId = roundId;
        round.winningOption = winningOption;
        emit RoundResolved(tournamentId, roundId, winningOption);
    }

    // this is called by an identity that wishes to vote on a given tournament, with the choices and weights
    function vote(uint tournamentId, uint[] memory choices, int[] memory weights) public {
        uint roundId = getCurrentRoundId(tournamentId);
        Tournament storage tournament = tournaments[tournamentId];
        Round storage currentRound = tournament.rounds[roundId];
        Identity storage id = identities[msg.sender];

        require(id.createdTime > 0, 'Must create identity first');
        require(roundId > id.lastRoundVoted[tournamentId], 'Can only vote one time per round');
        require(choices.length == weights.length, 'Mismatched choices and lengths');

        id.lastRoundVoted[tournamentId] = roundId;
        updateVoiceBalance(tournamentId, msg.sender);

        Vote storage currentVote = currentRound.votes[msg.sender];
        uint balance = tournament.voiceCredits[msg.sender];
        uint sum = 0;
        uint voiceSpend;
        for (uint i = 0; i < weights.length; i++) {
            currentVote.weights[choices[i]] = weights[i];
            voiceSpend = uint(weights[i] * weights[i]);
            currentRound.voiceTotals[choices[i]] = currentRound.voiceTotals[choices[i]].plus(voiceSpend);
            sum = sum.plus(voiceSpend);
        }
        require(sum <= balance, 'Must not spend more than your balance');

        emit VoteOccurred(tournamentId, roundId, msg.sender, choices, weights);
    }

    // GETTERS
    function getRound(uint tournamentId, uint roundId) public view returns (uint[2] memory) {
        Round memory round = tournaments[tournamentId].rounds[roundId];
        return [round.roundId, round.winningOption];
    }

    function isIdentity(address id) public view returns (bool)  {
        return identities[id].createdTime > 0;
    }

    function getVoiceCredits(uint tournamentId, address creditor) public view returns (uint) {
        return tournaments[tournamentId].voiceCredits[creditor];
    }

    function getLastRoundVoted(uint tournamentId, address voter) public view returns (uint) {
        return identities[voter].lastRoundVoted[tournamentId];
    }

    // this function computes the reward for an identity for the last round they participated in
    function getLastRoundReward(uint tournamentId, address ofAddress) public view returns (uint, uint) {
        Identity storage id = identities[ofAddress];
        return getRoundReward(tournamentId, id.lastRoundVoted[tournamentId], ofAddress);
    }

    function getRoundReward(uint tournamentId, uint roundId, address ofAddress) public view returns (uint, uint) {
        Tournament storage tournament = tournaments[tournamentId];
        Round storage round = tournament.rounds[roundId];
        Vote storage thisVote = round.votes[ofAddress];
        int weight = thisVote.weights[round.winningOption];
        uint bonus = uint(weight * weight);
        return (bonus, round.voiceTotals[round.winningOption]);
    }


}
