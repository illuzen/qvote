pragma solidity ^0.5.0;

import "./SafeMathLib.sol";

contract QVote {
    using SafeMathLib for uint;

    struct Topic {
        uint refId;
        uint topicId;
        address owner;
        uint votes;
        bytes32 descriptionHash; // ipfs hash of more verbose description, possibly multimedia
    }

    string symbol = 'VOX';
    string name = 'Voice Credits';
    uint public numTopics;
    uint public luckyNumber = 0;
    uint public lotterySize = 0;
    uint public winnings = 5;
    uint public winningFraction = 16;
    uint constant topicCost = 1;
    address public management;
    mapping (address => uint) public voxBalances;
    mapping (address => mapping (uint => bool)) public alreadyVoted;
    mapping (uint => Topic) public topicMap;

    event TopicCreated(uint indexed refId, uint topicId, address owner, bytes32 descriptionHash);
    event VoteOccurred(uint indexed topicId, address lastVoter, uint totalVotes);
    event Approval(address indexed owner, address indexed spender, uint value);
    event Transfer(address indexed from, address indexed to, uint value);

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(uint tokens, address mgmt) public {
        // need some vox to vote
        voxBalances[address(this)] = tokens;
        management = mgmt;
    }

    function changeManagement(address newManagement) managementOnly public {
        management = newManagement;
    }

    function changeWinnings(uint newWinnings) managementOnly public {
        winnings = newWinnings;
    }

    function changeWinningFraction(uint newWinningFraction) managementOnly public {
        winningFraction = newWinningFraction;
    }

    function createTopic(uint refTopicId, bytes32 descriptionHash) public {
        numTopics = numTopics.plus(1);
        voxBalances[msg.sender] = voxBalances[msg.sender].minus(topicCost);
        voxBalances[address(this)] = voxBalances[address(this)].plus(topicCost);
        topicMap[numTopics] = Topic(refTopicId, numTopics, msg.sender, 0, descriptionHash);
        emit Transfer(msg.sender, address(this), topicCost);
        emit TopicCreated(refTopicId, numTopics, msg.sender, descriptionHash);
    }

    function vote(uint topicId, uint votes) public {
        require(alreadyVoted[msg.sender][topicId] == false, 'Can only vote one time per topic');
        Topic storage t = topicMap[topicId];
        t.votes = t.votes.plus(votes);
        uint burnVotes = (votes * votes) - votes;
        voxBalances[msg.sender] = voxBalances[msg.sender].minus(votes * votes);
        voxBalances[address(this)] = voxBalances[address(this)].plus(burnVotes);
        voxBalances[t.owner] = voxBalances[t.owner].plus(votes);
        alreadyVoted[msg.sender][topicId] = true;
        emit Transfer(msg.sender, address(this), burnVotes);
        emit Transfer(msg.sender, t.owner, votes);
        emit VoteOccurred(topicId, msg.sender, t.votes);
    }

    function mint(address recipient, uint amount) public managementOnly {
        voxBalances[recipient] = voxBalances[recipient].plus(amount);
        voxBalances[address(this)] = voxBalances[address(this)].minus(amount);
        emit Transfer(address(this), recipient, amount);
    }

    function lottery(uint size) public managementOnly {
        require(voxBalances[address(this)] >= size, 'Must have enough vox to make lottery');
        luckyNumber = uint(blockhash(block.number)) % winningFraction;
        lotterySize = size;
    }

    function getLucky() public {
        if (uint(msg.sender) == luckyNumber) {
            voxBalances[msg.sender] = voxBalances[msg.sender].plus(winnings);
            voxBalances[address(this)] = voxBalances[address(this)].minus(winnings);
        }
    }

    function balanceOf(address owner) public view returns (uint balance) {
        return voxBalances[owner];
    }
}
