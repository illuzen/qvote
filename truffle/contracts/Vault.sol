pragma solidity ^0.5.0;

import "./FIN.sol";
import "./SafeMathLib.sol";


contract Vault {
    using SafeMathLib for uint;

    FIN public finToken;
    uint public startBlock;

    struct Tranche {
        uint id;
        address destination;
        uint totalCoins;
        uint currentCoins;
        uint lockPeriodBlocks;
        uint vestingPeriodEndBlock;
        uint lastWithdrawalBlock;
        uint startBlock;
    }

    // dmf gets treasury 300m reserves + 100m governance incentives + 200m liquidity incentives
    uint dmfAllocation = (300000000 + 100000000 + 200000000) * 1 ether;
    address public dmfAddress;

    // advisors get 50m
    uint advisorsAllocation = 50000000 * 1 ether;
    address public advisorsDao;

    // investors get 200m
    uint investorsAllocation = 200000000 * 1 ether;
    address public investorsDao;

    // team gets 150m
    uint teamAllocation = 150000000 * 1 ether;
    address public teamDao;

    mapping (uint => Tranche) public tranches;

    event CurrentWithdrawal(uint x);

    constructor(address dmf, address advisors, address investors, address team, address fin) public {
        dmfAddress = dmf;
        advisorsDao = advisors;
        investorsDao = investors;
        teamDao = team;
        finToken = FIN(fin);
        startBlock = block.number;

        uint investorLockPeriod = 0;
//        uint investorVestingPeriodEnd = startBlock + 1000000; // approximately 5 months
//        uint investorStartBlock = startBlock - 333333; // to give them 25% available up front
//        uint dmfLockPeriod = 1196307; // approximately 6 months
//        uint dmfVestingPeriodEnd = startBlock + 4785230;  // approximately 2 years
//        uint advisorsLockPeriod = 1196307; // approximately 6 months
//        uint advisorsVestingPeriodEnd = startBlock + 4785230;  // approximately 2 years
//        uint teamLockPeriod = 1196307; // approximately 6 months
//        uint teamVestingPeriodEnd = startBlock + 4785230;  // approximately 2 years

        uint investorVestingPeriodEnd = startBlock + 100; // for testing
        uint investorStartBlock = startBlock - 100; // for  testing

        uint dmfLockPeriod = 100; // for testing
        uint dmfVestingPeriodEnd = startBlock + 200;  // for testing

        uint advisorsLockPeriod = 100; // for testing
        uint advisorsVestingPeriodEnd = startBlock + 200;  // for testing

        uint teamLockPeriod = 100; // for testing
        uint teamVestingPeriodEnd = startBlock + 200;  // for testing

        tranches[1] = Tranche(1, dmfAddress, dmfAllocation, dmfAllocation, dmfLockPeriod, dmfVestingPeriodEnd, startBlock, startBlock);
        tranches[2] = Tranche(2, advisorsDao, advisorsAllocation, advisorsAllocation, advisorsLockPeriod, advisorsVestingPeriodEnd, startBlock, startBlock);
        tranches[3] = Tranche(3, investorsDao, investorsAllocation, investorsAllocation, investorLockPeriod, investorVestingPeriodEnd, investorStartBlock, investorStartBlock);
        tranches[4] = Tranche(4, teamDao, teamAllocation, teamAllocation, teamLockPeriod, teamVestingPeriodEnd, startBlock, startBlock);
    }

    function withdraw(uint trancheId) public {
        Tranche storage tranche = tranches[trancheId];
        require(block.number > tranche.lockPeriodBlocks + tranche.startBlock, 'Must wait until after lock period');
        require(tranche.currentCoins >  0, 'No coins left to withdraw');
        uint nowish = block.number;
        uint coinsPerBlock = tranche.totalCoins / (tranche.vestingPeriodEndBlock.minus(tranche.startBlock));
        if (nowish > tranche.vestingPeriodEndBlock) {
            nowish = tranche.vestingPeriodEndBlock;
        }
        uint currentWithdrawal = (nowish.minus(tranche.lastWithdrawalBlock)).times(coinsPerBlock);
        tranche.currentCoins = tranche.currentCoins.minus(currentWithdrawal);
        tranche.lastWithdrawalBlock = nowish;
        finToken.transfer(tranche.destination, currentWithdrawal);
    }
}
