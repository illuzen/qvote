pragma solidity ^0.5.0;

import "./FIN.sol";

contract DMF {

    address public management; // authoritative key that can make important decisions, can be DAO address later
    FIN public finToken;
    address public trollBox;

    event FundingOccurred(uint amount);

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(address mgmt, address fin, address troll) public {
        management = mgmt;
        finToken = FIN(fin);
        trollBox = troll;
    }

    function fundTrollbox(uint amount) public managementOnly {
        finToken.transfer(trollBox, amount);
        emit FundingOccurred(amount);
    }


}
