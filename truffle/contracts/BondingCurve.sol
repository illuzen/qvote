pragma solidity ^0.5.0;

import "./SafeMathLib.sol";
import "./FIN.sol";


/**
 * @title Bonding Curve
 * @dev Bonding curve contract based on Bacor formula
 * inspired by bancor protocol and simondlr
 * https://github.com/bancorprotocol/contracts
 * https://github.com/ConsenSys/curationmarkets/blob/master/CurationMarkets.sol
 */

contract BondingCurve {
    using SafeMathLib for uint;

    uint256 public poolBalance;
    uint256 public initialBalance;
    address public management;
    uint256 public initialPricePerToken;
    uint256 public finalPricePerToken;
    uint256 public inverseCoefficient;
    uint256 public maxPurchase;
    FIN public finToken;

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(address mgmt, uint initialPrice, uint finalPrice, uint balance, uint maxPurch) public {
        management = mgmt;
        initialPricePerToken = initialPrice;
        finalPricePerToken = finalPrice;
        initialBalance = balance;
        inverseCoefficient = initialBalance.times(initialBalance) / finalPrice;
        maxPurchase = maxPurch;
    }

    /**
     * @dev default function
     * gas ~ 91645
     */
    function() external payable {
        buy();
    }

    /**
     *
     *
     */
    function calculatePurchaseReturn(uint value) public view returns (uint) {
        uint balance = finToken.balanceOf(address(this));
        uint x = initialBalance - balance;
        uint price = initialPricePerToken + (x.times(x) / inverseCoefficient);
        return price.times(value);
    }

    /**
     * @dev Buy tokens
     * gas ~ 77825
     */
    function buy() public payable returns(bool) {
        require(msg.value > 0);
        require(msg.value <= maxPurchase);
        uint256 tokensToTransfer = calculatePurchaseReturn(msg.value);
        finToken.transfer(msg.sender, tokensToTransfer);
        return true;
    }

}