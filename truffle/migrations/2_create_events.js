const Trollbox = artifacts.require("Trollbox");

async function generateEvents(deployer, network, accounts) {
    trollbox = await Trollbox.deployed();
    await trollbox.createTournament('0x2bffe98db7ae36ac27dd176b5b4f1ddf1d319d2084570230ed75dfdca89030ea', 1, 5, 100, web3.utils.stringToHex('defi.cmc.eth'))
//  qvote = await QVote.deployed();
//  mgmt = await qvote.management();
//  await qvote.mint(accounts[0], 100, {from: mgmt});
//  await qvote.mint(accounts[1], 100, {from: mgmt});
//  await qvote.mint(accounts[2], 100, {from: mgmt});
//  await qvote.createTopic(0, '0x2bffe98db7ae36ac27dd176b5b4f1ddf1d319d2084570230ed75dfdca89030ea', {from: accounts[1]});
//  await qvote.createTopic(0, '0x818afba5d1684864bc06682b578410d509ed8e02a8b80953a203e12b7c88cf05', {from: accounts[1]});
//  await qvote.createTopic(0, '0xf15c7f02724b69b627534591a435ef04a108969291041832882638b14ace9f3a', {from: accounts[1]});
//  await qvote.createTopic(1, '0xea4d91931be760b9e7db715f88439d4fbe8678d969af9336efbe12501c6ad99f', {from: accounts[1]});
//  await qvote.vote(1, 1, {from: accounts[2]});
//  await qvote.vote(2, 2, {from: accounts[2]});
//  await qvote.vote(3, 3, {from: accounts[2]});
//  await qvote.vote(4, 4, {from: accounts[2]});
}

module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    await generateEvents(deployer, network, accounts);
  })
};

