const Migrations = artifacts.require("Migrations");
const SafeMathLib = artifacts.require("SafeMathLib");
const QVote = artifacts.require("QVote");
const Trollbox = artifacts.require("Trollbox");
const FIN = artifacts.require("FIN");
const DMF = artifacts.require("DMF");
const Vault = artifacts.require("Vault");

const numTokens =  '1000000000';

async function deployAll(deployer, network, accounts) {
      await deployer.deploy(SafeMathLib);
      await deployer.link(SafeMathLib, QVote);
      await deployer.link(SafeMathLib, Trollbox);
      await deployer.link(SafeMathLib, FIN);
      await deployer.link(SafeMathLib, Vault);
      await deployer.deploy(QVote, 1000000, accounts[0]);
      await deployer.deploy(FIN, accounts[0],  numTokens);
      const finToken = await FIN.deployed()
      await deployer.deploy(Trollbox, accounts[0], accounts[1], finToken.address);
      const trollBox = await Trollbox.deployed()
      await deployer.deploy(DMF, accounts[0], finToken.address, trollBox.address);
      await deployer.deploy(Vault, accounts[2], accounts[3], accounts[4], accounts[5], finToken.address);

}

module.exports = function(deployer, network, accounts) {
  deployer.then(async () => {
    await deployAll(deployer, network, accounts);

    });
};
